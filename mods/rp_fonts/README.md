# `rp_fonts`

This mod contains fonts for Repixture, to be used by other mods.

`rp_fonts` uses font files from GNU Unifont. The CSUR glyphs in the Private Use
Area have been intentionally left out.

The included font files are licensed under the
GNU General Public License, either Version 2 or (at your option) a later version,
with the exception that embedding the font in a document does not in itself constitute
a violation of the GNU GPL. The full terms of the license are in LICENSE.txt. 

You can find the homepage of GNU Unifont at:

<https://unifoundry.com/unifont/index.html>
