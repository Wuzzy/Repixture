msgid ""
msgstr ""
"Project-Id-Version: Luanti textdomain rp_hunger x.x.x\n"
"Report-Msgid-Bugs-To: Wuzzy@disroot.org\n"
"POT-Creation-Date: 2024-11-26 15:16+0100\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: mtt_convert 0.1.1\n"

#: mods/rp_hunger/init.lua:171
msgid "Hunger Debug:"
msgstr "Debug faim :"

#: mods/rp_hunger/init.lua:180
msgid "<hunger disabled>"
msgstr "<faim désactivée>"

#: mods/rp_hunger/init.lua:495
msgid "You are hungry."
msgstr "Vous avez faim."

#: mods/rp_hunger/init.lua:504
msgid "You are starving."
msgstr "Vous mourrez de faim."

#: mods/rp_hunger/init.lua:507
msgid "You starved to death."
msgstr "Vous êtes mort de faim."

#: mods/rp_hunger/init.lua:612
msgid "Eating"
msgstr "Alimentaire"

#: mods/rp_hunger/init.lua:613
msgid "You're eating food, which slows you down"
msgstr "Vous mangez, cela vous ralentit"

#: mods/rp_hunger/init.lua:634
msgid "Gourmet"
msgstr "Gourmet"

#: mods/rp_hunger/init.lua:635
msgid "Eat everything that can be eaten."
msgstr "Manger tout ce qui peut-être mangé."

#: mods/rp_hunger/init.lua:654
msgid "Set hunger level of player or yourself"
msgstr "Définit le niveau de faim d'un joueur ou de vous-même"

#. ~ Syntax for /hunger command. You may translate the words, but the symbols MUST be left intact.
#: mods/rp_hunger/init.lua:657
msgid "[<player>] <hunger>"
msgstr "[<joueur>] <faim>"

#: mods/rp_hunger/init.lua:664
msgid "Player is not online."
msgstr "Le joueur n'est pas connecté."

#: mods/rp_hunger/init.lua:678
msgid "No player."
msgstr "Pas de joueur."

#: mods/rp_hunger/init.lua:698
msgid "Food item"
msgstr "Nourriture"

#: mods/rp_hunger/init.lua:700
msgid "Food points: +@1"
msgstr "Points de nourriture : +@1"

#: mods/rp_hunger/init.lua:703
msgid "Saturation points: +@1"
msgstr "Points de saturation : +@1"
